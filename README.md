holiday_cz
==========

A Rust crate for finding out dates of Czech public holidays.

For usage see [docs](https://docs.rs/holiday_cz).

License
-------

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-apache](LICENSE-apache))
 * MIT license ([LICENSE-MIT](LICENSE-mit))

at your option.
